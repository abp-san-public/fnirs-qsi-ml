import pandas as pd
import numpy as np

from performance_utils import bootstrap_perf, compute_metrics
from sklearn.metrics import confusion_matrix

#%%
metadata_train = pd.read_csv('./dataset/metadata_train.csv', 
                             index_col=0)

metadata_test = pd.read_csv('./dataset/metadata_test.csv', 
                            index_col=0)


#%% 
CI_perf_train = bootstrap_perf(metadata_train['keep'], 
                               metadata_train['isgood'],
                               compute_metrics)

CI_perf_test = bootstrap_perf(metadata_test['keep'], 
                              metadata_test['isgood'],
                              compute_metrics)

CI_perf_train = pd.DataFrame(CI_perf_train, index=['median', '5%', '95%'], columns = ['MCC', 'precision', 'recall'])
CI_perf_test = pd.DataFrame(CI_perf_test, index=['median', '5%', '95%'], columns = ['MCC', 'precision', 'recall'])

print(CI_perf_train.transpose()) 
print(CI_perf_test.transpose())

print(confusion_matrix(metadata_train['keep'], 
                       metadata_train['isgood']))

print(confusion_matrix(metadata_test['keep'], 
                       metadata_test['isgood']))