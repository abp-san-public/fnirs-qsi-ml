import pandas as pd
import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
import seaborn as sns

#%%
metadata_train = pd.read_csv('./dataset/metadata_train.csv', index_col=0)
metadata_test = pd.read_csv('./dataset/metadata_test.csv', index_col=0)

metadata = pd.concat([metadata_train, metadata_test], axis=0)


#%%
fig, axes = plt.subplots(1,5, sharex=True)

for i_sqi, sqi_ind in enumerate(['sc', 'scp', 'cp', 'cvw', 'cv']):
    print(sqi_ind)
    print('{:.3f}'.format(metadata.query('keep == 0')[sqi_ind].median()))
    print('{:.3f}'.format(metadata.query('keep == 1')[sqi_ind].median()))
    print(stats.mannwhitneyu(metadata.query('keep == 0')[sqi_ind],
                             metadata.query('keep == 1')[sqi_ind]))
    
    plt.sca(axes[i_sqi])
    # plt.boxplot([features.query('keep == 0')[sqi_ind],
    #              features.query('keep == 1')[sqi_ind]])
    
    sns.violinplot(x='keep', y=sqi_ind, 
                   cut=0,
                   # scale='area',
                   data = metadata,
                   ax=axes[i_sqi])
    
    plt.xlabel('Signal Quality')
    plt.xticks([0,1], ['Bad', 'Good'])
    if i_sqi == 0:
        plt.ylabel('Value')
    else:
        plt.ylabel('')
    plt.title(sqi_ind)

#%%
sqi_names = ['sc', 'scp', 'cp', 'cvw', 'cv']
for i_sqi in np.arange(0, 4):
    for j_sqi in np.arange(i_sqi+1,5):
        SQI_I = sqi_names[i_sqi]
        SQI_J = sqi_names[j_sqi]
        print(SQI_I, SQI_J)
        print(stats.spearmanr(metadata[SQI_I], metadata[SQI_J]))