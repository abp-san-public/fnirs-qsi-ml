import pandas as pd
from performance_utils import bootstrap_perf, compute_metrics

#%%
metadata_train = pd.read_csv('./dataset/metadata_train.csv', 
                             index_col=0)

metadata_test = pd.read_csv('./dataset/metadata_test.csv', 
                            index_col=0)

ratings = pd.read_csv('./dataset/ratings.csv', 
                      index_col=0)

#%%

for rater in ['GE', 'MN', 'GG', 'RS']:
    print('----------------------')
    print(rater)
    
    ratings_others = ratings.copy().drop(columns=[rater, 'keep'])

    ratings_keep = ratings_others.mean(axis=1).round()
    ratings['keep_others'] = ratings_keep
    
    ratings_train = ratings.reindex(index=metadata_train.index)
    ratings_test = ratings.reindex(index=metadata_test.index)    
    
    CI_perf_train = bootstrap_perf(ratings_train['keep_others'], 
                                   ratings_train[rater],
                                   compute_metrics)
    
    CI_perf_test = bootstrap_perf(ratings_test['keep_others'], 
                                  ratings_test[rater],
                                  compute_metrics)
    
    CI_perf_train = pd.DataFrame(CI_perf_train, index=['median', '5%', '95%'], columns = ['MCC', 'precision', 'recall'])
    CI_perf_test = pd.DataFrame(CI_perf_test, index=['median', '5%', '95%'], columns = ['MCC', 'precision', 'recall'])
    
    print(CI_perf_train.transpose()) 
    print(CI_perf_test.transpose())
