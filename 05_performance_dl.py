import warnings
warnings.filterwarnings("ignore")

import pandas as pd
import numpy as np
np.random.seed(1234)
from performance_utils import bootstrap_perf, compute_metrics
from sklearn.metrics import confusion_matrix

results_train = pd.read_csv('./experiments/dl/features_train.csv', index_col=0)
results_test = pd.read_csv('./experiments/dl/features_test.csv', index_col=0)

CI_perf_train = bootstrap_perf(results_train['Target'], 
                               results_train['Predicted'],
                               compute_metrics)

CI_perf_test = bootstrap_perf(results_test['Target'], 
                              results_test['Predicted'],
                              compute_metrics)

CI_perf_train = pd.DataFrame(CI_perf_train, index=['median', '5%', '95%'], columns = ['MCC', 'precision', 'recall'])
CI_perf_test = pd.DataFrame(CI_perf_test, index=['median', '5%', '95%'], columns = ['MCC', 'precision', 'recall'])

print(CI_perf_train.transpose()) 
print(CI_perf_test.transpose())
print('------------------------;')

print(confusion_matrix(results_train['Target'], 
                       results_train['Predicted']))

print(confusion_matrix(results_test['Target'], 
                       results_test['Predicted']))

#%%