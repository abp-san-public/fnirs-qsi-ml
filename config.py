import numpy as np
import pandas as pd
from sklearn.svm import LinearSVC as SVC
from sklearn.ensemble import RandomForestClassifier
# from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn import clone


import sys

CV_N = 10
CV_K = 5

# features = features_train
# target = target_train
# model_params = get_model_params(params)
# model_get_importance = get_importance
# N=10
# feature_steps = 1

def get_SVM(n_features=None):
    params = {'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10, 100, 1000, 10000]}#, 1000]}

    def get_model(params, n_features):
        C = params['C']
        model = SVC(C=C, class_weight='balanced')
        return(model)

    def get_importance(model):
        return(abs(model.coef_[0,:]))
    
    return params, get_model, get_importance

def get_RF(n_features=None):
    params = {'n_estimators': [1, 5, 10, 50, 100, 250]}

    def get_model(params, n_features):
        n_estimators = params['n_estimators']
        
        model = RandomForestClassifier(n_estimators=n_estimators, 
                                       max_depth=3, 
                                       max_features='sqrt',
                                       min_samples_leaf=10,
                                       class_weight='balanced',
                                       n_jobs=-1)
        return(model)

    def get_importance(model):
        return(model.feature_importances_)
    
    return params, get_model, get_importance

def normalize_dataset(features_train, features_test):
    
    #% normalize features (based on train)
    scaler_features = StandardScaler()
    scaler_features.fit(features_train)
    
    features_train = pd.DataFrame(scaler_features.transform(features_train),
                                  index = features_train.index,
                                  columns = features_train.columns)
    
    features_test = pd.DataFrame(scaler_features.transform(features_test),
                                 index = features_test.index,
                                 columns = features_test.columns)
    
    return(features_train, features_test)
    
    



def borda_sort(X, isranking=True):
    #returns the indices that would sort the features
    #according to the borda count 
    X_rank = np.apply_along_axis(np.argsort, 1, X)
    
    n_feat = X_rank.shape[1]
    counts = np.zeros(n_feat)    
    for i_row in range(X_rank.shape[0]):
        for i_col in range(n_feat):
            counts[X_rank[i_row, i_col]] = counts[X_rank[i_row, i_col]] + n_feat - i_col
    
    if isranking:
        return(np.argsort(counts)[::-1])
    else:
        return(np.argsort(counts))
        
#%%                            #ranking score       #importance score
#X = np.array([[0,1,2,3,4,5],  #6, 5, 4, 3, 2, 1    #1, 2, 3, 4, 5, 6
#              [0,2,3,4,5,1],  #6, 4, 3, 2, 1, 5    #1, 3, 4, 5, 6, 2
#              [1,2,3,4,5,0]]) #5, 4, 3, 2, 1, 6    #2, 3, 4, 5, 6, 1
#                 total score 17,13,10, 7, 4,12     4, 8,11,14,17, 9
#                 rank        0,1,5,2,3,4           4,3,2,5,1,0
#print(borda_sort(X, True)) 
#print(borda_sort(X, False)) 

def get_data(drop_cols = [], dstype='sqi', normalize=False):
    if dstype == 'sqi':
        features_train = pd.read_csv('/home/bizzego/UniTn/data/fnirs_sqi/sexism/dl/labels_train.csv', index_col=0)
        features_train.drop(columns = ['isgood'], inplace = True)
        features_test = pd.read_csv('/home/bizzego/UniTn/data/fnirs_sqi/sexism/dl/labels_test.csv', index_col=0)
        features_test.drop(columns = ['isgood'], inplace = True)
    else:
        features_train = pd.read_csv('/home/bizzego/UniTn/data/fnirs_sqi/sexism/features_dl/dataset1/Baseline_noDO_train.csv', index_col=0)
        features_test = pd.read_csv('/home/bizzego/UniTn/data/fnirs_sqi/sexism/features_dl/dataset1/Baseline_noDO_test.csv', index_col=0)
    
    if len(drop_cols)>0:
        features_train.drop(columns = drop_cols, inplace = True)
        features_test.drop(columns = drop_cols, inplace = True)
        
    target_train = features_train.pop('keep')
    target_test = features_test.pop('keep')
    
    if normalize:
        norm_dataset = normalize_dataset(features_train, features_test)
        features_train = norm_dataset[0]
        features_test = norm_dataset[1]
                                     
    return({'features_train': features_train, 
            'target_train': target_train,
            'features_test': features_test, 
            'target_test': target_test})

def bootstrap_CI(x, K=0.25, N=1000):
    K = int(K*len(x))
    out = []
    for i in range(N):
        x_ = np.random.choice(x, K)
        out.append(np.mean(x_))
    return(np.percentile(out, 50), np.percentile(out, 5), np.percentile(out, 95))

def DAP(features, target, 
        get_model, model_params, model_get_importance,
        feature_steps,
        performance_metric, select_idx_best_metric):
    
    #create a vector with increasing number of features
    N_features = np.unique(np.linspace(1, features.shape[1], feature_steps).astype(int))
    #ensure last item includes all features
    N_features[-1] = features.shape[1]
    #reverse, so we start from all features
    N_features = N_features[::-1]
    
    
    features_prev_step = features
    results_dict = {}

    #for each feature step
    for N in N_features:
        features_step = features_prev_step.iloc[:, :N]
        target_step = target.copy()

        #% get numpy arrays and normalize
        X = features_step.values
        y = target_step.values

        #% create the K-fold splitter
        skf = KFold(n_splits=CV_K, shuffle=True)

        #for each param set
        results_N = {}

        for i_p, p in enumerate(model_params):
            #% set model template
            model_template = get_model(p, N)
            
            #%
            performances_10x5 = []    
            importances_10x5 = []

            #EXTERNAL CYCLE (CV_N times)
            for i_n in range(CV_N): #repeat CV_N times
                sys.stdout.write('\r {}, {}'.format(N, p))

                for idx_tr, idx_vl in skf.split(X, y): #for each fold
                    #create train and validation sets using k-fold splitting indexes
                    X_tr, y_tr = X[idx_tr, :], y[idx_tr]
                    X_vl, y_vl = X[idx_vl, :], y[idx_vl]

                    #initialize the model
                    model_cycle = clone(model_template)

                    #train
                    model_cycle.fit(X_tr, y_tr)

                    #extract and save IMPORTANCE
                    importance = model_get_importance(model_cycle)
                    importances_10x5.append(importance)

                    #simple prediction
                    y_tr_pred = model_cycle.predict(X_tr)
                    y_vl_pred = model_cycle.predict(X_vl)

                    # if (y_tr_pred == y_tr_pred[0]).all():
                    #     print('FLAG')
                        
                    #get performance
                    perf_tr = performance_metric(y_tr, y_tr_pred);
                    perf_vl = performance_metric(y_vl, y_vl_pred);

                    performances_10x5.append([perf_tr, perf_vl])
                    #END FOLD
                #END ITERATION


            #store peformances and importances for this C
            performances_10x5 = np.array(performances_10x5)
            importances_10x5 = np.array(importances_10x5)
            results_p = {'performances_10x5': performances_10x5,
                         'importances_10x5':  importances_10x5}
            
            results_N[i_p] = results_p
            #END p

        results_dict[N] = results_N

        #get the best performance across all ps based on performance_metric on valid

        #  get CI
        #CI_train = []
        CI_valid = []
        for i_p in range(len(model_params)):
            #performances_CI_train = bootstrap_CI(results_feat_step_C['performances_10x5'][:, 0])
            performances_CI_valid = bootstrap_CI(results_N[i_p]['performances_10x5'][:, 1])
            #CI_train.append(performances_CI_train)
            CI_valid.append(performances_CI_valid)
        CI_valid = np.array(CI_valid)

        #  get p with best perform
        idx_best_p = select_idx_best_metric(CI_valid[:,0])
        best_p = model_params[idx_best_p]

        # save the results
        results_dict[N]['best_p_idx'] = idx_best_p
        results_dict[N]['best_p'] = best_p
        results_dict[N]['best_p_CI'] = CI_valid[idx_best_p]


        #===========================
        # now rank features based on ranking on best C

        #get importances best p
        importances_best = results_N[idx_best_p]['importances_10x5']

        ranking_borda = borda_sort(importances_best, isranking=False)
        features_ranked = features_step.columns[ranking_borda]

        results_dict[N]['best_p_ranking'] = features_ranked

        features_prev_step = features_step[features_ranked]
        #END FEATURE STEP
        
    return(results_dict)
