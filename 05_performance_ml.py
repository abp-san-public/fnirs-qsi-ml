import warnings
warnings.filterwarnings("ignore")

import pandas as pd
from performance_utils import bootstrap_perf, compute_metrics, bootstrap_CI
import pickle

#%%
EXPERIMENT = 'rf' # 'rf'

resultsfile = f'./experiments/{EXPERIMENT}/results'

#%%
#% load results
with open(resultsfile, 'rb') as f:
    results_container = pickle.load(f)

results_DAP = results_container['DAP']


#% output best model
results_selection = results_container['selection']

#get best N
best_N = results_selection['best_N']

#get C for best N
best_p = results_selection['best_p']

#get ranking for best N
# features_ranked = results_selection['features_ranked']

print(f'Best N features: {best_N} - Best p: {best_p}')
# print(features_ranked)

#% compute performances
results_performance = results_container['performance']

#%%
CI_perf_train = bootstrap_perf(results_performance['true_train'], 
                               results_performance['pred_train'],
                               compute_metrics)

CI_perf_test = bootstrap_perf(results_performance['true_test'], 
                              results_performance['pred_test'],
                              compute_metrics)

CI_perf_train = pd.DataFrame(CI_perf_train, index=['median', '5%', '95%'], columns = ['MCC', 'precision', 'recall'])
CI_perf_test = pd.DataFrame(CI_perf_test, index=['median', '5%', '95%'], columns = ['MCC', 'precision', 'recall'])

print(CI_perf_train.transpose()) 
print(CI_perf_test.transpose())
