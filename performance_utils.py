import matplotlib.pyplot as plt
import numpy as np

from sklearn.metrics import matthews_corrcoef as mcc, confusion_matrix, recall_score, precision_score

def compute_metrics(y_true, y_pred):
    MCC = mcc(y_true, y_pred)
    # CM = confusion_matrix(y_true, y_pred)
    prec = precision_score(y_true, y_pred)
    rec = recall_score(y_true, y_pred)
    
    return(np.array([MCC, prec, rec]))

def plot_cm(true, pred):
    cm = confusion_matrix(true, pred)
    plt.imshow(cm)
    for ir in range(cm.shape[0]):
        for ic in range(cm.shape[1]):
            plt.text(ic, ir, cm[ir,ic], color='white', fontsize=14)

def bootstrap_perf(y_true, y_pred, function, K=0.25, N=1000, pred_value = None,
                   return_population = False):
    indices = np.arange(len(y_true))
    K = int(K*len(indices))
    
    out = []
    for i in range(N):
        idx_select = np.random.choice(indices, K)
        y_true_ = y_true[idx_select]
        y_pred_ = y_pred[idx_select]
        out.append(function(y_true_, y_pred_))
    
    out = np.array(out)
    
    if return_population:
        return(out)
    
    return([np.percentile(out, 50, axis=0), 
            np.percentile(out, 5, axis=0), 
            np.percentile(out, 95, axis=0)])
    


def bootstrap_CI(x, K=0.25, N=1000):
    K = int(K*len(x))
    out = []
    for i in range(N):
        x_ = np.random.choice(x, K)
        out.append(np.mean(x_))
    return(np.percentile(out, 50), np.percentile(out, 5), np.percentile(out, 95))