#IMPORT LIBRARIES
import torch
from torch.utils.data import DataLoader
import torch.nn as nn
import pandas as pd
import numpy as np
import os
import sys

import warnings
warnings.filterwarnings("ignore")

from nets import Simple

from dataset import SignalDataset

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


#%% SETTINGS
MODEL = Simple
BATCH_SIZE = 128 # FIX
LR = 1e-2 # FIX
EPOCHS = 200
N_CLASSES = 2
TARGET = 'keep'

OUTDIR = './experiments/dl'

#%%LOAD DATASETS
metadata_train = pd.read_csv('./dataset/metadata_train.csv', index_col=0)
dataset_train = SignalDataset(metadata_train, './dataset/signals_train', mode='train')
loader_train = DataLoader(dataset_train, batch_size=BATCH_SIZE, shuffle=True)

metadata_test = pd.read_csv('./dataset/metadata_test.csv', index_col=0)
dataset_test = SignalDataset(metadata_test, './dataset/signals_test', mode='test')
loader_test = DataLoader(dataset_test, batch_size=BATCH_SIZE, shuffle=True)

#%%
lab = [x[TARGET] for x in dataset_train]

_, n_lab = np.unique(lab, return_counts = True)
weights = (np.sum(n_lab) - n_lab)/np.sum(n_lab)

#%%
#INITIALIZE MODEL, CRITERION AND OPTIMIZER
model = MODEL(n_classes=N_CLASSES)
model.initialize_weights() 
model = model.to(device)

criterion = nn.CrossEntropyLoss(weight=torch.Tensor(weights)).to(device)

#%%
output_string = '\r Epoch {} - Loss: {:.4f}/{:.4f}'
#TRAIN
model.train()

losses_tr = []
losses_ts = []

optimizer = torch.optim.Adadelta(model.parameters(), lr=LR)

#parameters to divide the LR by 10 EVERY N_EPOCH*K epochs
#and update the LR every N_EPOCH
K = 1
N_EPOCH = 1000

for epoch in range(EPOCHS):
    
    if epoch % N_EPOCH == (N_EPOCH-1):
        optimizer = torch.optim.Adadelta(model.parameters(), lr=LR)
        LR/=np.power(10, 1/K)
        
    #for each batch in the dataset
    for j, batch in enumerate(loader_train):
        optimizer.zero_grad()
        model.train()
        signal = batch["signal"].to(device)
        target = batch[TARGET].to(device)
        output = model(signal) 
        loss = criterion(output, target) #compute loss
        loss.backward() #backward
        optimizer.step() #update weights
        loss_tr = loss.item()
        
        # check loss on valid
        if j % 5 == 0:

            losses_tr.append(loss_tr)
                        
            # with torch.no_grad():
            #     model.eval()
            #     batch_ts = next(iter(loader_test))
            #     signal_ts = batch_ts['signal'].to(device)
            #     target_ts = batch_ts[TARGET].to(device)
            #     output_ts = model.forward(signal_ts)
            #     loss_ts = criterion(output_ts,target_ts).item()
                
            #     losses_ts.append(loss_ts)

            #print status to stdout
            sys.stdout.write(output_string.format(epoch+1, 
                                                  losses_tr[-1], np.nan))#, losses_ts[-1]))

#%% save
import matplotlib.pyplot as plt

#loss
os.makedirs(OUTDIR, exist_ok=True)
plt.plot(losses_tr)
# plt.plot(losses_ts)
plt.savefig(f'{OUTDIR}/losses.png')

# weights
torch.save(model.state_dict(), f'{OUTDIR}/weights.pth')

