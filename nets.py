import torch.nn as nn

N_CH = 64
K = 21

class Simple(nn.Module):
    def __init__(self, n_classes=2):
        super(Simple, self).__init__()
        self.n_classes = n_classes
        
        self.conv_branch = nn.Sequential(
            nn.BatchNorm1d(2),
            
            nn.Conv1d(2, N_CH, K),
            nn.BatchNorm1d(N_CH),
            nn.ReLU(),
            nn.MaxPool1d(2),
            
            nn.Conv1d(N_CH, 2*N_CH, K),
            nn.BatchNorm1d(2*N_CH),
            nn.ReLU(),
            nn.MaxPool1d(2),
            
            nn.AdaptiveAvgPool1d(10)
            )


        self.linear = nn.Sequential(nn.Dropout(0.05), #FIX
                                    nn.Linear(10*2*N_CH, 1000),
                                    nn.ReLU(),
                                    nn.Dropout(0.05), #FIX
                                    nn.Linear(1000, 1000),
                                    nn.ReLU(),
                                    nn.Dropout(0.05), #FIX
                                    nn.Linear(1000, self.n_classes),
                                    nn.Softmax(1))
        
    def forward(self, x):
        # print(x.shape)
        x_feat = self.conv_branch(x)
        print(x_feat.shape)
        # print(x_feat.shape)
        x_feat = x_feat.view(x_feat.shape[0], -1)
        # print(x_feat.shape)
        x_out = self.linear(x_feat)
        return(x_out)

    def extract_features(self, x):
        x_feat = self.conv_branch(x)
        x_feat = x_feat.view(x_feat.shape[0], -1)
        return(x_feat)
        
    def initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Linear):
                nn.init.xavier_uniform_(m.weight)
                nn.init.constant_(m.bias, 0)
            if isinstance(m, nn.Conv1d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            if isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
