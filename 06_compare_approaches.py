import warnings
warnings.filterwarnings("ignore")

import pandas as pd
from performance_utils import bootstrap_perf, compute_metrics, bootstrap_CI
import pickle

import matplotlib.pyplot as plt


#%% SQIs threshold
metadata_test = pd.read_csv('./dataset/metadata_test.csv', 
                            index_col=0)

thr_test = bootstrap_perf(metadata_test['keep'], 
                          metadata_test['isgood'],
                          compute_metrics,
                          return_population=True)

thr_test = pd.DataFrame(thr_test)
thr_test['method'] = 'THR'

#%% SVM
resultsfile = './experiments/svm/results'

#% load results
with open(resultsfile, 'rb') as f:
    results_container = pickle.load(f)

#% compute performances
results_performance = results_container['performance']

svm_test = bootstrap_perf(results_performance['true_test'], 
                          results_performance['pred_test'],
                          compute_metrics,
                          return_population=True)

svm_test = pd.DataFrame(svm_test)
svm_test['method'] = 'SVM'

#%% RF
resultsfile = './experiments/rf/results'

#% load results
with open(resultsfile, 'rb') as f:
    results_container = pickle.load(f)

#% compute performances
results_performance = results_container['performance']

rf_test = bootstrap_perf(results_performance['true_test'], 
                          results_performance['pred_test'],
                          compute_metrics,
                          return_population=True)

rf_test = pd.DataFrame(rf_test)
rf_test['method'] = 'RF'

#%% DL
results_test = pd.read_csv('./experiments/dl/features_test.csv', index_col=0)
        
dl_test = bootstrap_perf(results_test['Target'], 
                         results_test['Predicted'],
                         compute_metrics,
                         return_population=True)

dl_test = pd.DataFrame(dl_test)
dl_test['method'] = 'DL'

#%%
data = pd.concat([thr_test, rf_test, svm_test, dl_test], axis=0)

import pingouin as pg

print(pg.anova(data, dv=0, between = 'method'))

paired = pg.pairwise_ttests(data, dv=0, between='method', return_desc=True)

#%%
data.boxplot(0, by='method')
