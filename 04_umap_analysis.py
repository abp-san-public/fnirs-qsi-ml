import umap
import numpy as np 
import sklearn.cluster as cluster
import pandas as pd
import matplotlib.pyplot as plt
from yellowbrick.cluster import KElbowVisualizer
import seaborn as sns

sns.set_style('darkgrid', {'legend.frameon':True})

features_train = pd.read_csv('./experiments/dl/features_train.csv', index_col=0)
features_test = pd.read_csv('./experiments/dl/features_test.csv', index_col=0)

features = pd.concat([features_train, features_test], axis=0)

idx_bad = np.where(features['Target']==0)[0]
idx_good = np.where(features['Target']==1)[0]

#%%
umapper = umap.UMAP(random_state=1234)
features_umap = umapper.fit_transform(features.values[:, :-2])

#%%
features_umap_pd = pd.DataFrame(features_umap, columns= ['Dimension 1', 'Dimension 2'])
features_umap_pd['Target'] = features['Target'].values
features_umap_pd['Predicted'] = features['Predicted'].values

features_umap_pd[['Target', 'Predicted']] = features_umap_pd[['Target', 'Predicted']].replace({0: 'Bad',
                                                                                               1: 'Good'})

sns.scatterplot(data=features_umap_pd,
                x='Dimension 1',
                y='Dimension 2',
                hue = 'Target',
                style='Predicted',
                palette = ['g', 'r'],
                legend='full')
plt.xlim(3.5, 16.5)
plt.ylim(8.5, 16)

#%%
features_umap_pd_bad = features_umap_pd.query('Target == "Bad"')
features_umap_pd_bad.index = features.iloc[idx_bad].index

#%% clustering on features
model = cluster.KMeans()

visualizer = KElbowVisualizer(model, k=(1,20))

visualizer.fit(features_umap_pd_bad[['Dimension 1', 'Dimension 2']])        # Fit the data to the visualizer
visualizer.show()        # Finalize and render the figure

#%%
kmeans_labels = cluster.KMeans(n_clusters=visualizer.elbow_value_).fit_predict(features_umap_pd_bad[['Dimension 1', 'Dimension 2']])

#%%
features_umap_pd_bad['Cluster'] = kmeans_labels+1

# features_umap_pd_bad['Cluster'] = features_umap_pd_bad['Cluster'].replace({2:3,
#                                                                            3:2})

sns.scatterplot(data=features_umap_pd_bad,
                x='Dimension 1',
                y='Dimension 2',
                hue = 'Cluster',
                style='Predicted',
                legend='full',
                palette = ['g', 'y', 'r'])

plt.xlim(3.5, 16.5)
plt.ylim(8.5, 16)

#%%
fig, axes = plt.subplots(visualizer.elbow_value_, 4, sharex='all', sharey='row')

for i_ax, i in enumerate([2,0,1]):
    signals = features_umap_pd_bad.query('Cluster == (@i+1)').index
    signals = np.random.permutation(signals)
    
    for j in range(min(4, len(signals))):
        try:
            sig = np.loadtxt(f'./dataset/signals_train/{signals[j]}.csv', delimiter = ',')
        except:
            sig = np.loadtxt(f'./dataset/signals_test/{signals[j]}.csv', delimiter = ',')
                
        sig = (sig - np.mean(sig)) / np.std(sig)
        
        axes[i_ax,j].plot(np.arange(450)/10, sig[:450])


for i in range(3):
    axes[i,0].set_ylabel('Normalized Amplitude')
    
for j in range(min(4, len(signals))):
    axes[2,j].set_xlabel('Time [s]')

