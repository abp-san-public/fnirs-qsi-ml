import pandas as pd
import os
import pingouin as pg
import numpy as np
from performance_utils import bootstrap_perf, compute_metrics

#%%
ratings = pd.read_csv('./dataset/ratings.csv', 
                      index_col=0)

#%%
ratings['targets'] = ratings.index

keep = ratings.pop('keep')

ratings_ICC = pd.melt(ratings, id_vars='targets')

ICC = pg.intraclass_corr(ratings_ICC, 
                         targets='targets',
                         raters='variable',
                         ratings='value')

#%%
#https://www.statology.org/intraclass-correlation-coefficient/
ICC2k = ICC.query('Type == "ICC2k"')
print(ICC2k[['ICC', 'F', 'df1', 'df2', 'pval', 'CI95%']])
