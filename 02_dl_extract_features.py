#IMPORT LIBRARIES
import torch
import pandas as pd

import numpy as np

from nets import Simple

from dataset import SignalDataset


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

state_dict = torch.load('./experiments/dl/weights.pth')


MODEL = Simple
N_CLASSES = 2
TARGET = 'keep'

#%%
#LOAD DATASETS
metadata_train = pd.read_csv('./dataset/metadata_train.csv', index_col=0)
dataset_train = SignalDataset(metadata_train, './dataset/signals_train', mode='test')

metadata_test = pd.read_csv('./dataset/metadata_test.csv', index_col=0)
dataset_test = SignalDataset(metadata_test, './dataset/signals_test', mode='test')

#%%
model = MODEL(n_classes=N_CLASSES)
model.load_state_dict(state_dict)
model = model.to(device)
model.eval()

#%%
samples_train = []
features_train = []
targets_train = []
predicted_train = []
with torch.no_grad():
    for sample in dataset_train:
        
        samples_train.append(sample['sample_id'])
        
        signal = sample["signal"].to(device)
        features = model.extract_features(signal.unsqueeze(0)) #forward
        features_train.append(features.cpu().numpy()[0])
        
        targets_train.append(sample[TARGET])
        
        output = model(signal.unsqueeze(0)) #forward
        _, pred = torch.max(output,1)
        
        predicted_train.append(pred.cpu().numpy()[0])

features_train = np.array(features_train)
features_train = pd.DataFrame(features_train, index = samples_train)

features_train['Target'] = targets_train
features_train['Predicted'] = predicted_train

features_train.to_csv('./experiments/dl/features_train.csv')

#%
samples_test = []
features_test = []
targets_test = []
predicted_test = []
with torch.no_grad():
    for sample in dataset_test:
        
        samples_test.append(sample['sample_id'])
        
        signal = sample["signal"].to(device)
        features = model.extract_features(signal.unsqueeze(0)) #forward
        features_test.append(features.cpu().numpy()[0])
        
        targets_test.append(sample[TARGET])
        
        output = model(signal.unsqueeze(0)) #forward
        _, pred = torch.max(output,1)
        
        predicted_test.append(pred.cpu().numpy()[0])

features_test = np.array(features_test)
features_test = pd.DataFrame(features_test, index = samples_test)

features_test['Target'] = targets_test
features_test['Predicted'] = predicted_test

features_test.to_csv('./experiments/dl/features_test.csv')
