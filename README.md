Code and data to reproduce results in:
A. Bizzego, M. Neoh, G. Gabrieli and G. Esposito, "A Machine Learning Perspective on fNIRS Signal Quality Control Approaches," in IEEE Transactions on Neural Systems and Rehabilitation Engineering, vol. 30, pp. 2292-2300, 2022, doi: 10.1109/TNSRE.2022.3198110. 
