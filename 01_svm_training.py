import warnings
warnings.filterwarnings("ignore")
import numpy as np
np.random.seed(1234)
import os
import pandas as pd
import pickle
import itertools

from sklearn.metrics import matthews_corrcoef as mcc
from config import normalize_dataset, DAP, get_SVM

#%%
def get_model_params(params):
    keys, values = zip(*params.items())
    permutations_dicts = [dict(zip(keys, v)) for v in itertools.product(*values)]
    return(permutations_dicts)

#%%
TARGET = 'keep'
N_STEPS = 1

results_container = {}

OUTDIR = './experiments/svm'

#%% SET MODEL PARAMS
params, get_model, get_importance = get_SVM()

#%%
performance_metric = mcc
select_idx_best_metric = np.argmax

#%% load data
metadata_train = pd.read_csv('./dataset/sqi_train.csv', index_col=0)
if 'isgood' in metadata_train.columns:
    metadata_train.drop(columns = ['isgood'], inplace = True)
if 'Predicted' in metadata_train.columns:
    metadata_train.drop(columns = ['Predicted'], inplace = True)
    
metadata_test = pd.read_csv('./dataset/sqi_test.csv', index_col=0)
if 'isgood' in metadata_test.columns:
    metadata_test.drop(columns = ['isgood'], inplace = True)
if 'Predicted' in metadata_test.columns:
    metadata_test.drop(columns = ['Predicted'], inplace = True)

#%%
target_train = metadata_train.pop(TARGET)
target_test = metadata_test.pop(TARGET)

norm_dataset = normalize_dataset(metadata_train, metadata_test)
features_train = norm_dataset[0]
features_test = norm_dataset[1]

#%%
results_dict = DAP(features_train, target_train, 
                   get_model, get_model_params(params), get_importance,
                   N_STEPS,
                   performance_metric = performance_metric, 
                   select_idx_best_metric = select_idx_best_metric)

results_container['DAP'] = results_dict

#%% get best p
CI_valid_ = []

# extract info for each feat step
for N in results_dict.keys():
    #best p
    idx_best_p = results_dict[N]['best_p_idx']
    p = results_dict[N]['best_p']
    CI_valid_.append(results_dict[N]['best_p_CI'])

#create nparray of CIs
CI_valid_ = np.array(CI_valid_)

#get best N
idx_best_N = select_idx_best_metric(CI_valid_[:,0])
best_N = list(results_dict.keys())[idx_best_N]

#get C for best N
best_p = results_dict[best_N]['best_p']

#get ranking for best N
features_ranked = results_dict[best_N]['best_p_ranking']

print(f'Best N features: {best_N} - Best p: {best_p}')

results_container['selection'] = {'best_N': best_N,
                                  'best_p': best_p,
                                  'features_ranked': features_ranked}

#%% train best model and get performance on train and test
model_best = get_model(best_p, best_N)

features_train_best = features_train.reindex(columns = features_ranked)
model_best.fit(features_train_best.values, target_train.values)
pred_train = model_best.predict(features_train_best.values)

features_test_best = features_test.reindex(columns = features_ranked)
pred_test = np.round(model_best.predict(features_test_best.values))

#%%
from sklearn.inspection import permutation_importance

r = permutation_importance(model_best, features_test_best.values, target_test.values,
                           n_repeats=30,
                           random_state=0)

importances = r['importances_mean']
idx_importances_ranked = np.argsort(importances)[::-1]
print(features_train_best.columns[idx_importances_ranked])


#%%
results_container['performance'] ={'pred_train': pred_train,
                                   'true_train': target_train.values,
                                   'pred_test': pred_test,
                                   'true_test': target_test.values,
                                   'model': model_best}

#%% save results_dict
os.makedirs(OUTDIR, exist_ok=True)

outfile = f'{OUTDIR}/results'

with open(outfile, 'wb') as f:
    pickle.dump(results_container, f)