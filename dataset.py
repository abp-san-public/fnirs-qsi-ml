import numpy as np
import torch
from torch.utils.data import Dataset
import os

FSAMP = 10
LENSECONDS = 20
NSAMP = LENSECONDS*FSAMP

def normalize(x):
    if np.std(x) != 0:
        return( (x - np.mean(x)) / np.std(x) )
    else:
        return(x - np.mean(x))

def augment(x):
    N = x.shape[0]
    x = normalize(x)
    
    t = np.stack([np.arange(len(x))/FSAMP for i in range(x.shape[1])], axis=1)
    
    #%%
    # fig, axes = plt.subplots(3, 1, sharex=True, sharey=True)
    # plt.sca(axes[0])
    # plt.plot(t, x)    
    
    random_std = np.random.uniform(0, 0.1, 2)
    
    #add random noise
    noise = np.random.normal([0, 0], random_std, (N,2))
    x_noise = x+noise
    # plt.sca(axes[1])
    # plt.plot(t, x)
    # plt.plot(t, x_noise)
    
    #add low freq noise
    
    # random_frq = np.random.uniform(0.001, 0.05, 2)
    # random_phs = np.random.uniform(-np.pi, np.pi, 2)
    # random_A = np.random.uniform(0, 1, 2)
    # random_frq = 0.1
    # random_phs = np.pi
    # random_A = 1
    
    # random_frq = np.array([random_frq, random_frq])
    # random_phs = np.array([random_phs, random_phs])
    # random_A = np.array([random_A, random_A])
    
    # sinusoid = random_A * np.sin(2*np.pi*random_frq*t + random_phs)
    # x_sin = x_noise+sinusoid
    
    # plt.sca(axes[2])
    # plt.plot(t, x_noise)
    # plt.plot(t, x_sin)
    
    #%%
    return(x_noise)
    
class SignalDataset(Dataset):
    def __init__(self, metadata, rootdir, seed = 123, mode='train'):
        np.random.seed(seed)
        self.metadata = metadata
        self.rootdir = rootdir
        self.mode = mode
        
    def __len__(self):
        return(self.metadata.shape[0])
    
    def __getitem__(self, idx):
        signal_row = self.metadata.iloc[idx, :]
        filename = signal_row.name
        signal_file = os.path.join(self.rootdir, f'{filename}.csv')
        
        #load signal
        signal = np.loadtxt(signal_file, delimiter = ',')

        #augmentation        
        if self.mode == 'train':
            # signal = augment(signal)
            
            #extract random LENSECONDS seconds
            random_start_idx = int(np.random.uniform(0, len(signal)-NSAMP))
            signal = signal[random_start_idx:random_start_idx+NSAMP]

        else:
            #extract LENSECONDS seconds
            idx_half = int(len(signal)/2)
            
            HALF_NSAMP = int(NSAMP/2)
            signal = signal[idx_half-HALF_NSAMP:idx_half-HALF_NSAMP+NSAMP]
        
        signal = normalize(signal)
        signal = torch.tensor(signal.T).float()
        
        return({'signal': signal, 
                'keep': int(signal_row['keep']),
                'sample_id': filename})


#import pandas as pd
#from torch.utils.data import DataLoader
#BASEPATH = '/home/giulio/Repositories/fnirsqc/Data'
#metadata_train = pd.read_csv(BASEPATH + '/Ratings/trainlabels.csv', index_col=0)
#dataset_train = SignalDataset(metadata_train, BASEPATH + '/Signals/raw/')
#
#for i in enumerate(dataset_train):
#    print(i)




